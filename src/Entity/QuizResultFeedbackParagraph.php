<?php

namespace Drupal\quiz_maker\Entity;

use Drupal\Component\Utility\Unicode;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Defines the QuizResultFeedbackParagraph entity class.
 */
class QuizResultFeedbackParagraph extends Paragraph {

  /**
   * {@inheritDoc}
   */
  public function label() {
    if ($this->bundle() === 'quiz_feedback_by_score') {
      $min = $this->get('field_min_score')->value;
      $max = $this->get('field_max_score')->value;
      $text = $this->get('field_feedback_text')->value;

      return $this->t('From @min% to @max%: @text', [
        '@min' => $min,
        '@max' => $max,
        '@text' => Unicode::truncate(strip_tags($text), 50, FALSE, TRUE),
      ]);
    }

    return parent::label();
  }

}
