<?php

namespace Drupal\quiz_maker\Trait;

use Drupal\quiz_maker\Entity\Question;

/**
 * Provides a scoring helper for question entity.
 *
 * @internal
 */
trait SimpleScoringQuestionTrait {

  /**
   * Implements SimpleScoringQuestionInterface::isSimpleScore().
   */
  public function isSimpleScore(): bool {

    if ($this->entity instanceof Question && $this->entity->hasField('field_simple_scoring')) {
      return (bool) $this->entity->get('field_simple_scoring')->getString();
    }
    return FALSE;
  }

}
