<?php

namespace Drupal\quiz_maker\EntityAccessControlHandler;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the quiz result entity type.
 *
 * phpcs:disable Drupal.Arrays.Array.LongLineDeclaration
 *
 * @see https://www.drupal.org/project/coder/issues/3185082
 */
final class QuizResultAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResult {
    return match($operation) {
      'view' => AccessResult::allowedIfHasPermissions($account, ['view quiz_result', 'administer quiz_result types'], 'OR'),
      'update' => AccessResult::allowedIfHasPermissions($account, ['edit quiz_result', 'administer quiz_result types'], 'OR'),
      'delete' => AccessResult::allowedIfHasPermissions($account, ['delete quiz_result', 'administer quiz_result types'], 'OR'),
      'view all revisions' => AccessResult::allowedIfHasPermission($account, 'view all quiz_result revisions'),
      'view revision' => AccessResult::allowedIfHasPermission($account, 'view quiz_result revision'),
      'revert revision' => AccessResult::allowedIfHasPermission($account, 'revert quiz_result revision'),
      'delete revision' => AccessResult::allowedIfHasPermission($account, 'delete quiz_result revision'),
      default => AccessResult::neutral(),
    };
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL): AccessResult {
    return AccessResult::allowedIfHasPermissions($account, ['create quiz_result', 'administer quiz_result types'], 'OR');
  }

}
